Name:		gamine
Version:	1.6
Release:	%mkrel 1
Summary:	An interactive game for young children
License:	GPLv3+
Group:		Education
URL:		https://sourceforge.net/projects/gamine-game
Source0:	https://sourceforge.net/projects/gamine-game/files/%{name}-%{version}.tar.gz
BuildRequires:	pkgconfig(gtk+-3.0)
BuildRequires:	pkgconfig(gstreamer-1.0)
BuildRequires:	hicolor-icon-theme
Requires:	hicolor-icon-theme

%description
Gamine is a game designed for 2 years old children who
are not able to use a keyboard. The child uses the mouse
to draw coloured dots and lines on the screen.
Let youngest children discover how to move mouse and use keyboard.

%prep
%setup -q

%build
%make_build

%install
%make_install
%find_lang %{name}

%files -f %{name}.lang
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_docdir}/%{name}
%{_datadir}/%{name}
%{_mandir}/man*/%{name}*
%{_iconsdir}/hicolor/scalable/apps/%{name}.svg
%config(noreplace) %{_sysconfdir}/%{name}.conf
